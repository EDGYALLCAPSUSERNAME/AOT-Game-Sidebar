#!/usr/bin/env python3

import re
import time
import praw
import inspect
import logging

# configure the logger
logging.basicConfig(filename='AOT_Game_Bot_Log.log',
                    level=logging.INFO,
                    format='%(asctime)s %(message)s')

# User config
# --------------------------------------------------------------------
# Filename to save to
FNAME = 'AOT_Game_Sidebar.txt'

# --------------------------------------------------------------------


# this function is formatting the dictionary of variables
# for logging in the event of an exception
def format_var_str(dic):
    s = ''
    for var, val in dic.items():
        s += ('\t' * 8) + '{}: {}\n'.format(var, val)

    return s


def get_sidebar_scoreboard(r):
    print('Getting scoreboard from sidebar...')
    sub = r.get_subreddit('attackontitangame')
    desc = sub.description

    # extract only the part we want
    regex = re.compile('\*\*\*(.*)\*\*\*', re.DOTALL)
    search = regex.search(desc)

    if search:
        return search.group(0)
    else:  # if it couldn't find it print it and raise an exception
        print('ERROR: Couldn\'t find score section in sidebar' )
        raise Exception('Regex match for score section not found')


def write_scoreboard_to_file(scores):
    print('Writing scores to file...')
    with open(FNAME, 'a') as f:
        f.write(scores + '\n\n')


def main():
    r = praw.Reddit(user_agent='/r/attackontitangame sidebar download v1.0 /u/cutety')

    try:
        score = get_sidebar_scoreboard(r)
        write_scoreboard_to_file(score)
    except Exception as e:  # catch any exception the bot could throw to log
        print('ERROR: {}'.format(e))

        # log the error event with specific information from the traceback
        traceback_log = '''
        ERROR: {e}
        File "{fname}", line {lineno}, in {fn}
        Time of error: {t}

        Variable dump:

        {g_vars}
        {l_vars}
        '''
        # grabs the traceback info
        frame, fname, lineno, fn = inspect.trace()[-1][:-2]
        # dump the variables and get formated strings
        g_vars = 'Globals:\n' + format_var_str(frame.f_globals)
        l_vars = 'Locals:\n' + format_var_str(frame.f_locals)

        logging.error(traceback_log.format(e=e, lineno=lineno, fn=fn,
                                           fname=fname, t=time.strftime('%c'),
                                           g_vars=g_vars, l_vars=l_vars))



if __name__ == '__main__':
    main()